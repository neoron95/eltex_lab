#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

struct matrix {
	int **mass;
	int size_a;
	int size_b;
};

void setmatrix (char* matrixFilename, struct matrix *a){
	FILE *matrixFile = fopen (matrixFilename, "rb");
	fscanf(matrixFile,"%d",&(a->size_a));
	fscanf(matrixFile,"%d",&(a->size_b));
	a->mass=malloc(a->size_a*sizeof(int*));
	
	for (int i=0; i < a->size_a; i++){
		a->mass[i]=malloc(a->size_b*sizeof(int));
		}
		
	for (int i=0; i < a->size_a; i++) {
		for (int j=0; j < a->size_b; j++) {
			fscanf(matrixFile,"%d",&(a->mass[i][j]));
		}
	}
	
	fclose (matrixFile);
}

void print(struct matrix *a){
	for (int i=0; i<a->size_a; i++) {
		for (int j=0; j<a->size_b; j++) {
			printf("%d ",a->mass[i][j]);
		}
		printf("\n");
	}

}

double MatWait(struct matrix *a){
	double mat=0;
	for (int i=0; i<a->size_a; i++) {
		for (int j=0; j<a->size_b; j++) {
			mat+=a->mass[i][j];
		}
	}
	
	mat/=(a->size_a+a->size_b);
	return mat;
}

void *thread_func(void *arg){
	pthread_mutex_lock(&mutex);
		struct matrix *Data= (struct matrix *) arg;
		
		double *r = (double*)malloc(sizeof(double)); 
		*r=MatWait(Data);
		
		sleep(rand() %3 + 1);
		
	pthread_mutex_unlock(&mutex);
	pthread_exit((void*)r);
}



int main(int argc, char* argv[]){
	
	int result;
	
	void ** MatW;
	MatW=malloc((argc-1)*sizeof(void*));
		
	struct matrix *a;
	a=malloc((argc-1)*sizeof(struct matrix));

	pthread_t *thread;
	thread=malloc((argc-1)*sizeof(pthread_t));
	
	for (int i=0; i<(argc-1); i++){
		setmatrix(argv[i+1],&a[i]);
	}

	for (int i=0; i<(argc-1); i++){
		printf("Massive %d\n",i);
		print(&a[i]);
		result=pthread_create(&thread[i], NULL, thread_func, &a[i]);
			if (result != 0) {
			perror("Creating the first thread");
			return EXIT_FAILURE;
			}
	}

	
	for (int i=0; i<(argc-1); i++){
		result = pthread_join(thread[i], &MatW[i]);
			if (result != 0) {
				perror("Joining the first thread");
				return EXIT_FAILURE;
			} else {
				printf("MatWait Massive %d is %f\n",i,*((double*)MatW[i]));
			}
	}
	
	//free
	for (int i=0; i<(argc-1); i++){ free(MatW[i]); }
	free(MatW);
	free(a);
	free(thread);
	return EXIT_SUCCESS;
	
}
