//arguments 1-count of bees

#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#include <time.h>

#define COUNT_BEE 5
#define FLAG 0
#define QTYPE 1

struct packet {
	int count;
	pid_t pid;
};

struct mymsgbuf {
	long mtype;
	struct packet mtext;
};

int bee () {
	sleep(rand()%2+3);
	int food = rand()%400;
	return food;
}



void child (int qid) {
		pid_t my=getpid();
		srand((unsigned) time(0)*my);
		struct mymsgbuf qbuf;
		
		while(1) {
		int tm=bee();
	
		printf ("Bee #%d add: %d\n",my, tm);

		qbuf.mtype=QTYPE;
		qbuf.mtext.count=tm;
		qbuf.mtext.pid=my;
		
			if((msgsnd(qid, (void *)&qbuf, sizeof(struct packet), FLAG)) ==-1){
				perror("msgsnd");
				exit(1);
			}
			
		}

}

int main(int argc, char *argv[]){
	
int num=atoi(argv[1]);
struct packet *mass;
key_t key;
int qid;

	mass=malloc(num*sizeof(struct packet));	
	key = ftok("/ect/hosts",'m');

	if((qid = msgget(key, IPC_CREAT|0660)) == -1) {
		perror("msgget");
		exit(1);
	}


	for (int i=0; i<num; i++){
		mass[i].pid = fork();
			if (mass[i].pid==-1) { printf("Error: %s", strerror(errno)); exit(-1);}
			else if (mass[i].pid==0) { child(qid); exit(-1);}
	}
		


}



