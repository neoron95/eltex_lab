// arguments 1 - count of eat; 2- time of sleep

#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#include <malloc.h>

#define COUNT_BEE 5
#define FLAG 0
#define QTYPE 1

struct packet {
	int count;
	pid_t pid;
};

struct mymsgbuf {
	long mtype;
	struct packet mtext;
};

void closeAll() {
system("killall clients");
printf("Bear is dead");
exit(-1);
	
}

int main(int argc, char *argv[]){
	
	int honey=100;
	int count=atoi(argv[1]);
	int sl=atoi(argv[2]);
	key_t key;
	int qid;
	struct mymsgbuf qbuf;

    key = ftok("/ect/hosts",'m');

		if((qid = msgget(key, IPC_CREAT|0660)) == -1) {
			perror("msgget");
			exit(1);
		}
       

	while(1) {
	
		struct msqid_ds info;
		msgctl(qid,IPC_STAT, &info);

		while(info.msg_qnum){
			msgrcv(qid, (struct mymsgbuf *)&qbuf, sizeof(struct packet), QTYPE, FLAG);
			honey+=qbuf.mtext.count;
			msgctl(qid,IPC_STAT, &info); 		
		}
		
		sleep(sl);
		honey-=count;
		
		printf ("Bear eat %d, Honey %d\n", count, honey);
		if (honey < 0) { closeAll();}
	}


}



