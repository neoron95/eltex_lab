#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>


union semun {
	 int val;                  /* значение для SETVAL */
	 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
	 unsigned short *array;    /* массивы для GETALL, SETALL */
							   /* часть, особенная для Linux: */
	 struct seminfo *__buf;    /* буфер для IPC_INFO */
};

struct matrix {
	int **mass;
	int size_a;
	int size_b;
};

struct vector {
	int *mass;
	int size;
};

void setmatrix (char* matrixFilename, struct matrix *a){
	FILE *matrixFile = fopen (matrixFilename, "rb");
	fscanf(matrixFile,"%d",&(a->size_a));
	fscanf(matrixFile,"%d",&(a->size_b));
	a->mass=malloc(a->size_a*sizeof(int*));
	
	for (int i=0; i < a->size_a; i++){
		a->mass[i]=malloc(a->size_b*sizeof(int));
		}
		
	for (int i=0; i < a->size_a; i++) {
		for (int j=0; j < a->size_b; j++) {
			fscanf(matrixFile,"%d",&(a->mass[i][j]));
		}
	}
	
	fclose (matrixFile);
}


void setvector (char* vectorFilename, struct vector *b){
	FILE *vectorFile = fopen (vectorFilename, "rb");
	
	fscanf(vectorFile,"%d",&(b->size));
	b->mass=malloc(b->size*sizeof(int));
	
	for (int i=0; i < b->size; i++) {
		fscanf(vectorFile,"%d",&(b->mass[i]));
	}
	
	fclose (vectorFile);
}


int main(int argc, char* argv[]){
	
	struct matrix a;
	struct vector b;
	struct vector c;


	setmatrix(argv[1],&a);
	setvector(argv[2],&b);

	
		
	
	
	pid_t pid[a.size_a];
	pid_t wpid;
	int status = 0;
	
	int shmid;
	key_t key = ftok(".", 'S');
	
	int *shm;
	
	int semid;
	union semun arg;
	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса


	
	//Creating semafores
	semid = semget(key, 1, 0666 | IPC_CREAT);
	
	//Install in sem #0 values 1
	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);
	 
	 //Create shared memory
	if ((shmid = shmget(key, sizeof(int*), IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		exit(1);
	}

	
	if (a.size_b!=b.size) { printf("Dimensions is differents");	exit(0); }
	else {
		c.size=a.size_a;
		c.mass=malloc(c.size*sizeof(int));	
			for (int i=0; i<a.size_a; i++){
				pid[i] = fork();
				srand(getpid());
				if (0 == pid[i]) {
					c.mass[i]=0;
					
					//Processing one pid
					for (int j=0; j<a.size_b; j++) {
						c.mass[i]+=a.mass[i][j]*b.mass[j];
					}
					
					/* Получим доступ к разделяемой памяти */
				if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
					perror("shmat");
					exit(1);
				}
				
				printf("[INFO] Proccess wait PID=%d i=%d\n", getpid(), i);
				fflush(stdout);
				
				//Lock	
				if((semop(semid, &lock_res, 1)) == -1){
					fprintf(stderr, "Lock failed\n");
					exit(1);
				} else{
					printf("[INFO] Semaphore resources decremented by one (locked) i=%d\n", i);
					fflush(stdout);
				}
				
				//Write values
				shm[i] = c.mass[i];
				sleep(rand() % 4);
				 
				//unlock
				if((semop(semid, &rel_res, 1)) == -1){
					 fprintf(stderr, "Unlock failed\n");
					 exit(1);	
				} else{
					printf("[INFO] Semaphore resources incremented by one (unlocked) i=%d\n", i);
					fflush(stdout);
				}
				
				printf("Math string matrix [i=%d] = %d\n", i, shm[i]);
				fflush(stdout);
				
				 //Unconnected 
				if (shmdt(shm) < 0) {
					printf("Failed unconneting\n");
					exit(1);
				}
				exit(0);
			}	else if (pid[i]<0){
					perror("fork");
					exit(1); 
			}
				
			}
			
		for (int i = 0; i < a.size_a; i++) {
			wpid = waitpid(pid[i], &status, 0);
			if (pid[i] == wpid) {
				printf("Proccess %d done,  pid=%d\n", i, wpid);
				fflush(stdout);
				}
		}
		
		/* Получим доступ к разделяемой памяти */
	if ((shm = (int*)shmat(shmid, NULL, 0)) == (int *) -1) {
		perror("shmat");
		exit(1);
	}

	printf("MASSIVE A\n");
	for (int i=0; i<a.size_a; i++) {
		for (int j=0; j<a.size_b; j++) {
			printf("%d ",a.mass[i][j]);
		}
		printf("\n");
	}
	
	printf("MASSIVE B\n");
	for (int i=0; i<b.size; i++){
			printf("%d\n",b.mass[i]);
		}
	
	printf("MASSIVE C\n");
	for (int i=0; i<c.size; i++){
		printf("%d\n",shm[i]);
	}

	if (shmdt(shm) < 0) {
		printf("Failed unconneting\n");
		exit(1);
	} 
	
	// Delete created objects IPC	
	 if (shmctl(shmid, IPC_RMID, 0) < 0) {
		printf("Failed delete\n");
		exit(1);
	}
	
	if (semctl(semid, 0, IPC_RMID) < 0) {
		printf("НFailed delete semafore\n");
		exit(1);
	}
		
		
	}

	
	
}
