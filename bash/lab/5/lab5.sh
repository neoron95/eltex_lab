#!/bin/bash

#Написать скрипт разбора логов, скрипт запрашивает имя процесса и 
#ищет для него логи, вводит их на экран за указанный промежуток времени. 
#Скрипт по очереди проверяет наличие файла с именем процесса в папке 
#/var/log/, если файл найден — проверяет его на наличие в нем 
#требуемой информации, если его нет, проверяет наличие папки 
#с имененм процесса, при нахождении папки скрипт просматривает 
#все файлы в этой папке на предмет нахождения в ней требуемой информации, 
#если не найдена папка, то информация ищется в syslog..


proc="kern";

m_start="Jul";
d_start=17;
h_start=9;
min_start=18;
s_start=0;

m_fin="Dec";
d_fin=24;
h_fin=10;
min_fin=59;
s_fin=59;


function showFile(){
echo "$index"
i=0;
	while [ "$i" -ne "$index" ]
	do
		#echo "${find_arr[$i]}";
		#awk -F'[: ]' '$2 >= $d_start && $2 <= $d_fin && $3>=$h_start && $3<=$h_fin && $4>=$min_start && $4<=$min_fin && $5>=$s_start && $5<=s_fin { print }' ${find_arr[$i]}
		#awk '$2 >= $d_start { print }' ${find_arr[$i]}
		
		#unzip if it is gz
		
		#gz=$(echo "${find_arr[$i]" | grep ".gz$");
		#if [ "$gz" -ne "" ] 
		#	then 
		#		gunzip ${find_arr[$i]}; 
		#		find_arr[$i]=$(echo "${find_arr[$i]}" | sed 's/.gz//');	
		#		unzip[]=""
		#fi;
		
		
		
		while read line
		do
				month=$(echo "$line" | sed 's/ .*//');
				day=$(echo "$line" | sed 's/^[a-zA-Z]* //' | sed 's/ .*//');
				hour=$(echo "$line" | sed 's/^[a-zA-Z]* [0-9]* //'| sed 's/:/ /' |sed 's/ .*//');
				min=$(echo "$line" | sed 's/:/ /g' | sed 's/^[a-zA-Z]* [0-9]* [0-9]* //' |sed 's/ .*//');
				sec=$(echo "$line" | sed 's/:/ /g' | sed 's/^[a-zA-Z]* [0-9]* [0-9]* [0-9]* //'| sed 's/ .*//');
				
				now=$(date -d "$hour:$min:$sec $month $day" +%s);
				if ((now>date_start && now<date_fin)) 
				then echo $line;
				fi;
		done < ${find_arr[$i]};
	
	echo "$i";
	i=$(($i+1));
	done;


}

function find_syslog() {
	find_arr[1]="/var/log/syslog"
	
}


function find_path_log(){
index=0;
	while read line
	do
			find_arr[$index]="$line";
			index=$(($index+1));
	done < <(find /var/log -name "$proc*");
	
	#showFile;
	if [ "$index" -eq "0" ]
		then find_syslog;
		else showFile;
	fi;
}

function find_file_log(){
index=0;
	while read line
	do
			find_arr[$index]="$line";
			index=$(($index+1));
	done < <(find /var/log -maxdepth 1 -type f -name "$proc*");
	
	if [ "$index" -eq "0" ]
		then find_path_log;
		else showFile;
	fi;
}


#Input information
function menu(){
	echo '0.Exit'
	echo '1.Select proccess'
	echo '2.Select start date'
	echo '3.Select finish date'
	echo '4.Show logs'
	echo '5.Show logs without date'
}

function status(){
	echo "Your proccess is: $proc"
	echo "Start date"
	echo "Month: $m_start"
	echo "Day: $d_start"
	echo "Hour: $h_start"
	echo "Minute: $min_start"
	echo "Second: $s_start"
	echo "Finish date"
	echo "Month: $m_fin"
	echo "Day: $d_fin"
	echo "Hour: $h_fin"
	echo "Minute: $min_fin"
	echo -e "Second: $s_fin\n"
}

function start_date(){
	echo "Start date"
	echo "Write number of month"
	read m_start;
	month_to_num $m_start 0;
	echo "Write day"
	read d_start;
	echo "Write hour"
	read h_start;
	echo "Write minute"
	read min_start;
	echo "Write second"
	read s_start;
}

function fin_date(){
	echo "Finish date"
	echo "Write number of month"
	read m_fin;
	month_to_num m_fin 1;
	echo "Write day"
	read d_fin;
	echo "Write hour"
	read h_fin;
	echo "Write minute"
	read min_fin;
	echo "Write second"
	read sec_fin;
}

function month_to_num(){

	case "$1" in
			"1")
			month="Jan"
			;;
			"2")
			month="Feb"
			;;
			"3")
			month="Mar"
			;;
			"4")
			month="Apr"
			;;
			"5")
			month="May"
			;;
			"6")
			month="Jun"
			;;
			"7")
			month="Jul"
			;;
			"8")
			month="Aug"
			;;
			"9")
			month="Sept"
			;;
			"10")
			month="Oct"
			;;
			"11")
			month="Nov"
			;;
			"12")
			month="Dec"
			;;
	esac;

	if [ "$2" -eq "0" ]
		then m_start=$month;
		else m_fin=$month;
	fi;
}



function showAll() {
	find_file_log;
}

while [ 0 -ne 1 ]
do
	status;
	menu;
	date_start=$(date -d "$h_start:$min_start:$s_start $m_start $d_start" +%s);
	date_fin=$(date -d "$h_fin:$min_fin:$s_fin $m_fin $d_fin" +%s);
	read i;
		case "$i" in
				"0")
				exit;
				;;
				"1")
				select_proc;
				;;
				"2")
				start_date;
				;;
				"3")
				fin_date;
				;;
				"4")
				show;
				;;
				"5")
				showAll;
				;;
		esac;
done;



#awk -F'[: ]' '$2 >= $m_start && $2 <= $m_fin && $3>=$h_start && $3<=$h_fin 
				#&& $4>=$min_start && $4<=$min_fin && $5>=$s_start && $5<=s_fin 
				#{ print }'

#awk '$2 >= $m_start && $2 <= m_fin  { print }'
#egrep '(Jul [17|18]  0[89]|1[0-9]|2[012]:)' kern.log

#Conditions

#Seconds
#egrep -e'(Aug)' kern.log | egrep -e '($2 >=)'

#date -d "15:24:23 Oct 21" +%s

#awk -F'[: ]' '($(date -d "$4:$5:$6 $1 $3" +%s) > 150000)' kern.log 

#date_start=$(date -d "17:37:00 Aug 2" +%s);
#awk -F, -v date="$(date -d "$4:$5:$6 $1 $3" +%s)" '$date>$date_start'

#awk -F'[: ]' -v "date_start=$(date -d "17:37:00 Aug 2" "+%s")" '{ de=date -d "$4:$5:$6 $1 $3" "+%s"; print $0 }' kern.log

#awk -F'[: ]' -v "date_start=$(date -d "17 37 00 Aug 2" +%s)" -v "dt=$(date -d "$4 $5 $6 $1 $3" +%s)" '{ if (dt>date_start) print }' kern.log

#awk -F '[: ]' '{ print $4:$5:$6 $1 $3 }' kern.log
#egrep -e '()'

#awk -F '[: ]' -v "dt=$(date -d "$4:$5:$6 $1 $3" "+%s")" '{  print dt }' kern.log
