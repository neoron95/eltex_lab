#!/bin/bash

#Написать скрипт, который проверяет смонтирована ли папка по nfs и 
#при необходимости ее монтирует, в папку он помещает файл, в котором написано: 
#имя фамилия ip адрес время запуска\останова uptime. 
#скрипт должен выполняться при запуске и останове машины. 
#Для выполнения задания необходимо пользоваться вохможностями upstart и system.d


folder="/home/nikita/import";

ip=$(hostname -I);
ip=($ip);
num=$((${#ip[*]}-1))
ip=${ip[$num]};

mount | grep $folder > dev/null;
if [ "$?" -ne "0" ]
	then 
		mount -t nfs 192.168.101.248:/home/nfs $folder;
fi;

echo "Nagornov Nikita $ip $(date) $(uptime)" > $folder/$ip;
