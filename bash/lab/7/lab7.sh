#!/bin/bash

#Написать скрипт, который проанализировав установленное ПО 
#в системе синтезирует правила файрвола и применит их.

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

iptables -F

function add_rule() {

iptables -t filter -A INPUT -p tcp -m tcp --tcp-flags SYN,ACK,RST SYN --dport $1 -j ACCEPT;
echo  -e "[Accepting]\t role for $2";

}


function set_port() {
name=$1;
port=$2;	
	
dpkg -s $name 2>/dev/null | grep Status | grep installed >/dev/null;

if [ "$?" -eq "0" ]
	then echo -e "${GREEN}[Installed]${NC}\t $name"; add_rule $port $name;
	else echo -e "${RED}[Not Intalled]${NC}\t $name";
fi;

}


while read line;
do
	set_port $line;
done < ports;

iptables -t filter -A INPUT -p tcp -m tcp --tcp-flags SYN,ACK,RST SYN -j DROP
