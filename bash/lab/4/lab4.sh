#!/bin/bash

#Написать скрипт, который контролирует выполнение какого-либо процесса и 
#в случае прерывания этого процесса возобновляет его работу. 
#Скрипт должен запросить периодичность проверки и имя процесса. 
#Скрипт должен иметь функции: создания, редактирования и удаления заданий контроля. 
#Сторонние задания cron, скрипт должен игнорировать(не показывать). 
#Для выполнения задания НЕ разрешается создавать другие скрипты
#(скрипт должен запускаемый из крона и скрипт запускаемый пользователем 
#должен быть один.).


mode='find-proccess-scripts';

function image(){
	echo '0.Exit'
	echo '1.Create'
	echo '2.Edit'
	echo '3.Delete'
	echo '4.Show tasks'
}


#Show crontab
function readall() {

unset cron_file;

index=0
while read line; do
    array[$index]="$line"
	a=$(echo "${array[$index]}");
	cron_file[$index]=$a; 
    
    index=$(($index+1))
done < <(crontab -l);	
	
}

function readcron() {
unset alarm;
unset str_alarm;


index=0;
count=0;
while read line; do
    array[$index]="$line"
	a=$(echo -e "${array[$index]}"| grep "find-proccess-scripts" | sed 's/#find-proccess-scripts//' | sed 's/\*/\\052/g');
	if [ -n "$a" ]
	then 
		str_alarm[$count]=$index;
		alarm[$count]=$a; 
		count=$(($count+1)); 
	fi;
    
    index=$(($index+1))
done < <(crontab -l);	
	
}

function show() {
readall;
readcron;
for ((a=0; a < ${#alarm[*]}; a++))
do
    echo -e "$a: ${alarm[$a]}" | sed 's/-c //'
done	
}

#Add new cron
function create() {
	pw=$(pwd);
	path=$(echo $0 | sed 's/\.//');
	path=$pw$path;
	
	echo '1.Add script';
	echo '2.Add proccess';
	read i;

	case "$i" in

			"1")
			echo 'Write name of script';
			read nameproc;
			echo 'Write path of script';
			read pathproc;
			;;
			
			"2")
			echo 'Write name of proccess';
			read nameproc;
			pathproc='';
			;;
	esac;

	echo 'Write settings of periodic';
	echo '(use * if you need not this set)';
	echo 'Write minutes';
	read m;
	echo 'Write hours';
	read h;
	echo 'Write days';
	read dom;
	echo 'Write days of month';
	read mon;
	echo 'Write days of week';
	read dow;

	
new_alarm=$(echo -e "$m $h $dom $mon $dow $path $nameproc -c #$mode $pathproc" | sed 's/\*/%/g');

#echo $new_alarm;

if [ "$1" == "1" ]
	then add_alarm $new_alarm;
	else {
		echo 'Write number of edited taks'
		read num;
		return $num;
		}
fi;


}

function add_alarm() {
	
	> file.cnf;
for ((a=0; a < ${#cron_file[*]}; a++))
	do
	 echo -e "${cron_file[$a]}" >> file.cnf;
done	
	echo -e "$new_alarm" | sed 's/%/\*/g' >> file.cnf;
	
	crontab file.cnf;
}

#Edit
function edit() {
	
> file.cnf;

create;
num=$?;

for ((a=0; a < ${#cron_file[*]}; a++))
	do
		if [ "$a" != "${str_alarm[$num]}" ]
		then echo -e "${cron_file[$a]}" >> file.cnf;
		else echo -e "$new_alarm" | sed 's/%/\*/g' >> file.cnf;
		fi;
   
done	
	crontab file.cnf;
	
	
}

#delete
function delete () {

> file.cnf;
for ((a=0; a < ${#cron_file[*]}; a++))
	do
		if [ "$a" != "${str_alarm[$1]}" ]
		then echo -e "${cron_file[$a]}" >> file.cnf;
		fi;
   
done	
	crontab file.cnf;
	
}



function user() {


	while [ 1 -ne 0 ]
	image;
	read i;
	do
		case "$i" in
				
				"0")
				exit;
				;;
				
				"1")
				create 1;
				;;
				
				"2")
				edit;
				;;
						
				"3")
				echo 'Write deleted task'
				read tmp;
				delete $tmp;
				;;
				
				"4")
				show;
				;;
		esac
	done;
}


function script() {

my_proc=$1;	
	
pathproc=$(crontab -l | grep "find-proccess-scripts" | grep "$my_proc" | sed 's/^.*#find-proccess-scripts //' | sed 's/\*/\\052/g');
	

find=$(ps -aux | grep "$my_proc" | grep -v grep | grep -v "$0");
	
	if [[ "$find" == "" ]]
		then
			if [[ "$pathproc" == "" ]]
				then $my_proc;
				else $pathproc/$my_proc;
			fi;
	fi;
	
}

if [[ "$2" == "-c" ]]
	then script $1;
	else user;
fi;
