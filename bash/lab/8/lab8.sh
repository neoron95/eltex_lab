#!/bin/bash

#Написать скрипт для резервного копирования указанной информации 
#в указанное место, с указанием периодичности\либо времени запуска. 
#Скрипт должен иметь удобный для человека интерфейс. 
#Резервное копирование должно осуществляться как средствами dd, tar и rsync. 
#Предусмотреть работу скрипта без интерфейса пользователя, т.е. 
#Возможность при которой  все конфигурирование может осуществляться 
#через конфигурационный файл. Конфигурационный файл должен иметь 
#удобочитаемый для пользователя формат.

cron_tmp=cron.bcp;

function settings() {
	echo "Write path"
	read path;
	echo "Write path to archivation file"
	read path_arch;
	echo "Write period of arhivation (cron format)"
	read time;
}


function dir() {
	echo "Select the type of archivation"
	echo "1.Tar"
	echo "2.Rsync"
	read i;
	
	case "$i" in	
			"1")
			echo "$time tar -cf \"$path_arch\$(date).tar\" $path" >> $cron_tmp;
			echo "$time tar -cf $path_arch $path";
			;;
			
			"2")
			echo "$time rsync -avhr $path \"$path_arch\$(date)\"" >> $cron_tmp;
			;;
			
			*)
			exit;
			;;
	esac;
			
}


if [[ "$1" == "-f" ]]
	then source config;
	else settings;
fi;

crontab -l > $cron_tmp;

if [ -b $path ]
	then echo "$time dd if=$path of=\"$path_arch\$(date).dd\"" >> $cron_tmp;
	else if [ -d $path ]
		then dir;
		else echo "Error"
	fi;
fi;

crontab $cron_tmp;
rm $cron_tmp;








